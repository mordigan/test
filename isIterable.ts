/** @format */

const isIterable = (
  arg:
    | string
    | number
    | boolean
    | null
    | undefined
    | { [k: string]: any }
    | Array<string | number | boolean | null | undefined>
): boolean => (arg ? arg.hasOwnProperty('length') : false);

export default isIterable;
