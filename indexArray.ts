/** @format */

const present = (arr: number[], n: number): boolean => arr.some((i) => i === n);

/* 
  I just know the standard library of Javascript.
*/

export default present;
