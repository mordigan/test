import fizzBuzz from '../fizbuzz';
import { expect } from 'chai';
describe('fizbuzz', () => {
    it('can start without parameters', () => {
        const res = fizzBuzz();
        expect(res.length).to.not.be.equal(0);
    });
    it('can\'t work if first parameter is greater then the second', () => {
        expect(() => fizzBuzz(10, 1)).to.throw('Invalid arguments');
    });
    it('cat\'t work if start is less then 0', () => {
        expect(() => fizzBuzz(-1, 1)).to.throw('Invalid arguments');
    });
    it('cat\'t work if end is less then 0', () => {
        expect(() => fizzBuzz(1, -13)).to.throw('Invalid arguments');
    });
});
