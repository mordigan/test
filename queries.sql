select u.email from payment as p
    left join users as u on p.user_id = u.id
where successful is true
group by u.email;

select u.email from payment as p
    left join users as u on p.user_id = u.id
where successful is false
group by u.email;

select count(p), u.id, u.email from users as u
    left join payment as p on u.id = p.user_id and p.successful is true
group by u.id;
