/** @format */

const mapper = (
    obj: {
    userId: number;
    firstName: string;
    lastName: string;
    bookTitle: string;
    bookId: number;
  }[]
) => {
    const tempResult: { [k: number]: any } = {};

    const mappedResult: any[] = [];
    for (const u of obj) {
        const books = tempResult[u.userId]
            ? [...tempResult[u.userId].books, { id: u.bookId, title: u.bookTitle }]
            : [{ id: u.bookId, title: u.bookTitle }];
        tempResult[u.userId] = {
            id: u.userId,
            firstName: u.firstName,
            lastName: u.lastName,
            books,
        };
    }

    for (const k of Object.keys(tempResult))
        mappedResult.push(tempResult[Number(k)]);

    return mappedResult;
};

export default mapper;
