/** @format */
import mapper from './objMap';
import isIterable from './isIterable';
import present from './indexArray';
import validateString from './superman';
import parse from './phoneNumberSanitizer';
import excluder from './exclude';
import fizzBuzz from './fizbuzz';
console.log(isIterable(null));
console.log(isIterable('hello world'));
console.log(isIterable({ 1: 2, 3: 4 }));
console.log(isIterable([1, 2, 3, 4, 5]));
const mappedObject = mapper([
    {
        userId: 1,
        firstName: 'john',
        lastName: 'doe',
        bookId: 1,
        bookTitle: 'don quixote',
    },
    {
        userId: 1,
        firstName: 'john',
        lastName: 'doe',
        bookId: 3,
        bookTitle: 'robinson crusoe',
    },
    {
        userId: 1,
        firstName: 'john',
        lastName: 'doe',
        bookId: 4,
        bookTitle: 'gulliver\'s travels',
    },
    {
        userId: 1,
        firstName: 'john',
        lastName: 'doe',
        bookId: 5,
        bookTitle: 'frankenstein',
    },
    {
        userId: 1,
        firstName: 'john',
        lastName: 'doe',
        bookId: 6,
        bookTitle: 'the count of monte cristo',
    },

    {
        userId: 2,
        firstName: 'richard',
        lastName: 'roe',
        bookId: 2,
        bookTitle: 'pilgrim\'s progress',
    },
    {
        userId: 2,
        firstName: 'richard',
        lastName: 'roe',
        bookId: 6,
        bookTitle: 'the count of monte cristo',
    },

    {
        userId: 3,
        firstName: 'john',
        lastName: 'smith',
        bookId: 1,
        bookTitle: 'don quixote',
    },
    {
        userId: 3,
        firstName: 'john',
        lastName: 'smith',
        bookId: 2,
        bookTitle: 'pilgrim\'s progress',
    },
    {
        userId: 3,
        firstName: 'john',
        lastName: 'smith',
        bookId: 5,
        bookTitle: 'frankenstein',
    },
    {
        userId: 3,
        firstName: 'john',
        lastName: 'smith',
        bookId: 7,
        bookTitle: 'jane eyre',
    },
]);
console.log(mappedObject);

const arr: number[] = [];

for (let i = 0; i < 50; i++) if (i % 2 === 0) arr.push(i);
console.log(present(arr, 0));

console.log(present(arr, 1));

console.log(validateString('Superman is awesome!'));

console.log(parse('(123) 45-6 789-9', '---'));

console.log(
    excluder(
        [
            '/src/common/api.js',
            '/src/common/preferences.js',
            '/src/styles/main.css',
            '/src/styles/base/_base.scss',
            '/src/assets/apple-touch-icon-57.png',
        ],
        ['/src/styles/', '/src/assets/apple-touch-icon-57.png']
    )
);
console.log(fizzBuzz(1, 100));
