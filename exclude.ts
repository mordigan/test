const excluder = (
    include: Array<string | null>,
    exclude: string[]
): string[] => {
    for (const e of exclude) {
        for (let i = 0; i < include.length; i++) {
            const val = include[i];
            if (!val) continue;
            if (val.includes(e)) include[i] = null;
        }
    }
    const finalArr: string[] = [];
    include.forEach((i) => (i !== null ? finalArr.push(i) : null));
    return finalArr;
};

export default excluder;
