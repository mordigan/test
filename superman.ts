/** @format */

function validateString(str: string): number {
    // str = ' ' + str
    const index = str.toLowerCase().indexOf('superman');
    if (!index && index !== 0) {
        throw new Error('String does not contain superman');
    }
    return index;
}

/* 
  This happens because str.toLowerCase().indexOf('superman') in this case returns 0, which in javascript is false
  !0 == !false == true, and in case if it's true `throw new Error('String does not contain superman');` code works
  There are multiple ways of fixing this. for example we can add one line in the beginning of the function
  which will add an empty space in the beginning of the str `str = ' ' + str`
  Or we can do what I did.
 */
export default validateString;
