/** @format */

const skipSpecialCharacters = (str: string) => {
  const special = '!@#$%^&*()\\\'"/?>-=< '; // eslint-disable-line
    let final = '';
    for (let i = 0; i < str.length; i++) {
        let isSpecial = false;
        for (let j = 0; j < special.length; j++) {
            if (str[i] === special[j]) isSpecial = true;
        }
        if (!isSpecial) final += str[i];
    }
    return final;
};

const parse = (phoneNum: string, delimiter = '-'): string => {
    const sanitized = skipSpecialCharacters(phoneNum);
    return `${sanitized.substr(0, 3)}${delimiter}${sanitized.substr(
        3,
        3
    )}${delimiter}${sanitized.substr(6, 4)}`;
};

export default parse;
